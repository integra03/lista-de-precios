<?php
	include("../../Archivos/conectar_bd.php");
    session_start();
	$conexion1 = $_SESSION['conexion'];
	$datos=new bd($conexion1);
    set_error_handler("my_warning_handler", E_ALL);

	function my_warning_handler($errno, $errstr, $errfile, $errline, $errcontext) {
		throw new Exception( $errstr );
	}

	if (isset($_POST['idlista'])) {
	    $idlista = $_POST['idlista'];
        $codtercero = $_POST['codtercero'];        
      
        //Inactivamos todas las listas pertenecientes a dicho tercero
        $sqldesaclistas = "update ClientesListaPrecios set EstadoClienteListaP='INACTIVO' where codtercero='".$codtercero."'"; 
        
       try{
          if($datos->consulta($sqldesaclistas)){
           	     $sqlactlista="update ClientesListaPrecios set EstadoClienteListaP='ACTIVO' where codtercero='".$codtercero."' and IdEncListaPrecio=".$idlista;
	           	 $datos->consulta($sqlactlista);
	           	  echo json_encode(array(
				    'success' => true,
			 	    'razon' => 'Lista activada satisfactoriamente'			    
		          ));  
           }else{
           	   echo json_encode(array(
			    'success' => false,
			    'razon' => 'Error al activar lista'			    
	           ));  
           }
            
       }catch (Exception $e) {       	
       	   echo json_encode(array(
			    'success' => false,
			    'razon' => 'Error al activar lista'			    
	       ));  
       }
	}else{
		 echo json_encode(array(
			    'success' => false,
			    'razon' => 'Error al activar lista'			    
	     ));  
	}
?>