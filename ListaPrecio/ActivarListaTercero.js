if(grid.getSelectionModel().selections.length>0){
	            var selrecord = grid.getSelectionModel().getSelected();
                var idenclista = selrecord.get('idenclistaprecio');
                var codtercero=selrecord.get('codtercero');

                  Ext.Ajax.request({
		            url:'Aplicaciones/ListaPrecio/ActivarLista.php',
		            params:{
		               idlista:idenclista,
		               codtercero:codtercero
		            },
		            success:function(response){
                       var respuesta=Ext.decode(response.responseText);
		               if(respuesta.success){
                       	  grid.getStore().reload();
                          Ext.MessageBox.show({
                                    title: "Exito",
                                    msg:'Lista Clonada Exitosamente.',
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.INFO
                            });	
			            }else{
			            	 Ext.MessageBox.show({
                                    title: "Información",
                                    msg:respuesta.razon,
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.INFO
                            });	
			            }
		            },
		            failure:function(){
		                Ext.MessageBox.show({
		                    title: "Error",
		                    msg:'Ocurrió un error al activar la lista seleccionada. Por favor, Vuelva a intentar',
		                    buttons: Ext.MessageBox.OK,
		                    icon: Ext.MessageBox.ERROR
		                });   
		            }
		        });
}else{
	 Ext.MessageBox.show({
        title: 'Advertencia',
        msg: 'Debe seleccionar la lista a activar.',
        buttons: Ext.MessageBox.OK,
        icon: Ext.MessageBox.WARNING
    });
}
   