

if(grid.getSelectionModel().selections.length>0){	 

var selrecord = grid.getSelectionModel().getSelected();
var idenclista = selrecord.get('idenclistaprecio');

var storeTercero = new Ext.data.JsonStore({
    url: 'Aplicaciones/ListaPrecio/CargaGrillaTercero.php',
    remoteSort:true,
    totalProperty: 'total', 
    baseParams: {
    	idlista:'',
        codtercero:'',
        nomtercero:''
    },
    root: 'datosterc',
    fields: ['codtercero','nom_terc']
});


Ext.override(Ext.grid.RowSelectionModel, {
    forceCtrl: false,
    handleMouseDown : function(g, rowIndex, e){
    if(e.button !== 0 || this.isLocked()){
    return;
    }
    var view = this.grid.getView();
    if(e.shiftKey && !this.singleSelect && this.last !== false){
    var last = this.last;
    this.selectRange(last, rowIndex, e.ctrlKey || this.forceCtrl);
    this.last = last;
    view.focusRow(rowIndex);
    }else{
    var isSelected = this.isSelected(rowIndex);
    if((e.ctrlKey || this.forceCtrl) && isSelected){
    this.deselectRow(rowIndex);
    }else if(!isSelected || this.getCount() > 1){
    this.selectRow(rowIndex, e.ctrlKey || this.forceCtrl || e.shiftKey);
    view.focusRow(rowIndex);
    }
    }
    }
});

var gridTercero= Ext.id();
var smterc = new Ext.grid.CheckboxSelectionModel({
    singleSelect:false,
    forceCtrl:true
});

var boxcodigo = new Ext.BoxComponent({
    autoEl: {
        html: 'Código: '
    }
});

var boxnombre = new Ext.BoxComponent({
    autoEl: {
        html: '&nbsp;&nbsp;Nombre: '
    }
});

var btnBuscarTerc = new Ext.Button({
    icon:'images/buscar.png',
    cls:"x-btn-text-icon",
    text:'Filtrar',
    id:'btnBuscarTerc',
    hidden:false,
    handler:function(){
        Ext.getCmp(gridTercero).getStore().removeAll();
        Ext.getCmp(gridTercero).getStore().baseParams.idlista=idenclista;
        Ext.getCmp(gridTercero).getStore().baseParams.codtercero=Ext.getCmp('Buscar_Texto_Cod_Terc').getValue(); 
        Ext.getCmp(gridTercero).getStore().baseParams.nomtercero=Ext.getCmp('Buscar_Texto_Nom_Terc').getValue(); 
        Ext.getCmp(gridTercero).getStore().load(); 
    }
});

var btnQuitarFiltroTerc = new Ext.Button({
    icon:'images/buscar_quitar.png',
    cls:"x-btn-text-icon",
    text:'Quitar Filtro',
    id:'btnQuitarFiltroTerc',
    hidden:false,
    handler:function(){
        Ext.getCmp('Buscar_Texto_Cod_Terc').setValue(""); 
        Ext.getCmp('Buscar_Texto_Nom_Terc').setValue("");
        Ext.getCmp(gridTercero).getStore().removeAll();
        Ext.getCmp(gridTercero).getStore().baseParams.idlista=idenclista;
        Ext.getCmp(gridTercero).getStore().baseParams.codtercero=''; 
        Ext.getCmp(gridTercero).getStore().baseParams.nomtercero=''; 
        Ext.getCmp(gridTercero).getStore().load(); 
    }
});

var Pcl_Terceros = new Ext.grid.GridPanel({
    id: gridTercero,
    store: storeTercero,
    height:450,
    width:485,
    loadMask:new Ext.LoadMask(Ext.getBody(), {msg:"Cargando terceros..."}),
    region:'center',
    columns: [
		    smterc,
		    {
		        header:'CodTercero',
		        width:50,
		        dataIndex:'codtercero',
		        sortable: true
		    },        
		    {
		        header:'Nombre Tercero',
		        width:230,
		        dataIndex:'nom_terc',
		        sortable: true
		    }
     ],
    clicksToEdit: 2,
    border: true,
    stripeRows: true,
    sm:smterc,
    viewConfig: {
        forceFit: true
    },
    bbar: new Ext.PagingToolbar({
        pageSize: 50,
        store: storeTercero,
        displayInfo: true,
        displayMsg: '{0} - {1} de {2} Registros',
        emptyMsg: "No hay registros",
        listeners: {
            beforechange:function(el,e){
            
            var tb = el;
            var o = {
                start:e.start,
                limit:e.limit
            };
            
               if(Pcl_Terceros.getSelections().length>0){ 
                    
                  Ext.Msg.show({
                            title:'Confirmación',
                            msg: 'Recuerde que si cambia de página se perderá la selección ¿Desea continuar?',
                            buttons: Ext.Msg.YESNO,
                            fn: function (btn){
                                if(btn=='yes'){     
                                    tb.store.load({params:o});
                                }
                            },
                            icon: Ext.Msg.QUESTION
                        });    
                 
             return false;              
                }
            }
        }
    }),
    listeners: {

    }
});


var vnListadoTerceros = new Ext.Window({
    title:'Listado de Terceros',
    border: true,
    closable:true,
    closeAction:'hide',
    layout:'form',
    width: 500,
    height:545,
    modal:true,
    monitorValid:true,
    tbar:['  ',boxcodigo, {
        xtype: 'textfield',       
        width: 95,
        emptyText: 'Texto...',
        enableKeyEvents: true,
        id: 'Buscar_Texto_Cod_Terc'
    },boxnombre,{
        xtype: 'textfield',       
        width: 160,
        emptyText: 'Texto...',
        enableKeyEvents: true,
        id: 'Buscar_Texto_Nom_Terc'
    },btnBuscarTerc, btnQuitarFiltroTerc ],
    items:[Pcl_Terceros],
    buttonAlign: 'center',
    buttons:[
    {
        text:'Aceptar',
        handler:function(){
            var sel = Ext.getCmp(gridTercero).getSelections();            
            if(sel.length>0){
                    var maxterceros=Ext.getCmp(gridTercero).getStore().reader.jsonData.maxtercero;
                   if(sel.length<=maxterceros){
                        AsignarTerceros(sel);
                  }else{
                        Ext.MessageBox.show({
                            title: 'Advertencia',
                            msg: 'El numero de terceros seleccionados excede el limite establecido',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                  }
            }else{
                        Ext.MessageBox.show({
                            title: 'Advertencia',
                            msg: 'No ha seleccionado ningun tercero para asignar a la lista',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
            }
        }
    },
    {
        text:'Cerrar',
        handler:function(){
            vnListadoTerceros.hide();
         }
    }
   ]

});   

function AsignarTerceros(seleccionados){
            mascara.show();
            var datos = [];
            for(var i = 0; i < seleccionados.length; i++){
                 datos.push({
                    codtercero:seleccionados[i].data.codtercero
                 });
            }
            var datosTerceros = Ext.encode(datos);
            Ext.Ajax.request({
                    url : 'Aplicaciones/ListaPrecio/AsignarListaTercero.php',
                    params :{
                       idlista:idenclista,
                       terceros:datosTerceros
                    },
                    success : function(response) {
                         grid.getStore().reload();
                         Ext.getCmp(gridTercero).getStore().baseParams.idlista=idenclista;                               
                         Ext.getCmp(gridTercero).getStore().load();
                         /*vnListadoTerceros.hide();*/                                        
                        var respuesta=Ext.decode(response.responseText);
                        if(respuesta.success){
                              Ext.Msg.alert('Información','Terceros asignados satisfactoriamente');
                        }else{
                              Ext.Msg.alert('Información',respuesta.razon);
                        }
                         mascara.hide();
                    },
                    failure: function(){
                        Ext.Msg.alert('Error','Error al asignar terceros a la lista, intente más tarde');
                        mascara.hide();
                    }
            });
}
            
	    vnListadoTerceros.show();	   
	   /* Ext.getCmp(gridTercero).el.mask('Cargando terceros...', 'x-mask-loading');*/
	    Ext.getCmp(gridTercero).getStore().baseParams.idlista=idenclista;
        Ext.getCmp(gridTercero).getStore().baseParams.codtercero=''; 
        Ext.getCmp(gridTercero).getStore().baseParams.nomtercero='';  	   
	    Ext.getCmp(gridTercero).getStore().load();      
       /* Ext.getCmp(gridTercero).el.unmask();*/
	   
}else{
	 Ext.MessageBox.show({
        title: 'Advertencia',
        msg: 'Debe seleccionar la lista primero',
        buttons: Ext.MessageBox.OK,
        icon: Ext.MessageBox.WARNING
    });
}



