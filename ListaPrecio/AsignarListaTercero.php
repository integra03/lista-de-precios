<?php
	include("../../Archivos/conectar_bd.php");
    session_start();
	$conexion1 = $_SESSION['conexion'];
	$datos=new bd($conexion1);
    set_error_handler("my_warning_handler", E_ALL);

	function my_warning_handler($errno, $errstr, $errfile, $errline, $errcontext) {
		throw new Exception( $errstr );
	}

	if (isset($_POST['idlista'])) {
	    $idlista = $_POST['idlista'];
        $datosTerc = $_POST['terceros'];        
      
        $sqlinsterc='';
        $datosTerceros = json_decode($datosTerc, true);
        for ($i = 0; $i < count($datosTerceros); $i++) {
              $sqlinsterc .= "insert into ClientesListaPrecios(CodTercero,IdEnclistaPrecio, EstadoClienteListaP) 
              values ('" . $datosTerceros[$i]['codtercero'] . "'," . $idlista . ",'INACTIVO');"; 
        }
       try{
           $datos->consulta($sqlinsterc);
            echo json_encode(array(
			    'success' => true,
			    'razon' => 'Terceros asignados satisfactoriamente'			    
	       ));  
       }catch (Exception $e) {       	
       	   echo json_encode(array(
			    'success' => false,
			    'razon' => 'Error al asignar terceros a la lista'			    
	       ));  
       }
	}else{
		 echo json_encode(array(
			    'success' => false,
			    'razon' => 'Error al asignar terceros a la lista'			    
	     ));  
	}
?>