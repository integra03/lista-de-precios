<?php
	include("../../Archivos/conectar_bd.php");
    session_start();
	$conexion1 = $_SESSION['conexion'];
	$datos=new bd($conexion1);
	set_error_handler("my_warning_handler", E_ALL);

	function my_warning_handler($errno, $errstr, $errfile, $errline, $errcontext) {
		throw new Exception( $errstr );
	}	

	if ( isset($_POST['idlista'])) {
	    $idlista = $_POST['idlista'];
	    $nombre = $_POST['nombre'];
        $sqlcopialistaprecio="exec procClonarListaPrecio '".$nombre."',".$idlista;

        try {
           if ($datos->consulta($sqlcopialistaprecio)==true) {
             echo json_encode(array(
			    'success' => true,
			    'razon' => 'Lista clonada exitosamente'			    
			 ));
          
           }else{
         	 echo json_encode(array(
			    'success' => false,
			    'razon' => 'Error al clonar la lista'			    
			 ));           
           }
		}catch (Exception $e) {
		    echo json_encode(array(
			    'success' => false,
			    'razon' => 'El nombre ingresado ya existe'		    
	        ));  
		}   
       
	}else{
		 echo json_encode(array(
			    'success' => false,
			    'razon' => 'Error al clonar la lista'			    
	     ));  
	}
?>