
if(grid.getSelectionModel().selections.length>0){
		Ext.Msg.prompt('Ingrese Nombre de la lista', 'Por favor, Ingrese el nombre con que desea guardar la lista a clonar:', function(btn, text){
		    if (btn == 'ok'){
		        var selrecord = grid.getSelectionModel().getSelected();
                var idenclista = selrecord.get('idenclistaprecio');
                Ext.Ajax.request({
		            url:'Aplicaciones/ListaPrecio/ClonarLista.php',
		            params:{
		                idlista:idenclista,
		                nombre:text
		            },
		            success:function(response){
		              var respuesta=Ext.decode(response.responseText);
		               if(respuesta.success){
                       	  grid.getStore().reload();
                          Ext.MessageBox.show({
                                    title: "Exito",
                                    msg:'Lista Clonada Exitosamente.',
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.INFO
                            });													                                       
			              
			            }else{
			            	 Ext.MessageBox.show({
                                    title: "Exito",
                                    msg:respuesta.razon,
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.INFO
                            });	
			            }
		            },
		            failure:function(){
		                Ext.MessageBox.show({
		                    title: "Error",
		                    msg:'Ocurrió un error al clonar la lista. Por favor, Vuelva a intentar',
		                    buttons: Ext.MessageBox.OK,
		                    icon: Ext.MessageBox.ERROR
		                });   
		            }
		        });
		    }
		});
}else{
	 Ext.MessageBox.show({
        title: 'Advertencia',
        msg: 'Debe seleccionar un elemento de la lista.',
        buttons: Ext.MessageBox.OK,
        icon: Ext.MessageBox.WARNING
    });
}
   