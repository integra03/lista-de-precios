
var storeLista = new Ext.data.JsonStore({
    url: 'Aplicaciones/ListaPrecio/CodigoOpciones/CargaListaPrecios.php',
    autoLoad:true,
    remoteSort:true,
    totalProperty: 'total',  
     baseParams: {        
        codtercero:'',
        nomtercero:''
    },  
    root: 'datoslista',
    fields: ['idlista','nombre','iddetlista']
});

storeLista.load();

var storeTercero = new Ext.data.JsonStore({
    url: 'Aplicaciones/ListaPrecio/CodigoOpciones/CargaTercerosLista.php',
    remoteSort:true,
    totalProperty: 'total', 
    baseParams: {
        idlista:'',
        codtercero:'',
        nomtercero:''
    },
    root: 'datosterc',
    fields: ['codtercero','nom_terc','espredeterminada']
});

var storeArticulo = new Ext.data.JsonStore({
    url: 'Aplicaciones/ListaPrecio/CodigoOpciones/CargaArticulosLista.php',
    remoteSort:true,
    totalProperty: 'total', 
    baseParams: {
    	idlista:'',
        codarticulo:'',
        nomarticulo:''
    },
    root: 'datosart',
    fields: ['codarticulo','nomarticulo','precio','idestado']
});

/*storeTercero.load();*/

var gridViewlp = new Ext.grid.GridView({    
    getRowClass : function (row, index) { 
       var iddetlista  = row.get('iddetlista');
        if(iddetlista === null) {
              return '';
        }else{
              return 'rojoclaro';
       }       
   } 
});  


var gridlp= Ext.id();
var Pcl_ListaP = new Ext.grid.GridPanel({
    id: gridlp,
    store:storeLista,
    title:'Lista Precio',
    view:gridViewlp,
   /*height:450,
    width:485,*/
    loadMask:new Ext.LoadMask(Ext.getBody(), {msg:"Cargando listas de precios..."}),
    region:'center',
    columns: [		    
		    {
		        header:'Id',
		        width:50,
		        dataIndex:'idlista',
		        sortable: true
		    },        
		    {
		        header:'Descripcion',
		        width:340,
		        dataIndex:'nombre',
		        sortable: true
		    }
    ],
    clicksToEdit: 2,
    border: true,
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    bbar: new Ext.PagingToolbar({
	    pageSize: 50,
	    store: storeLista,
	    displayInfo: true,
	    displayMsg: '{0} - {1} de {2} Registros',
	    emptyMsg: "No hay registros"          
    }),
    listeners: {
          rowclick:function(grid,rowIndex,e) {
              var selrecord=grid.getSelectionModel().getSelected();
              var idlista=selrecord.get('idlista');
              Ext.getCmp(gridTercero).getStore().removeAll();
              Ext.getCmp(gridTercero).getStore().baseParams.idlista=idlista;
              Ext.getCmp(gridTercero).getStore().baseParams.codtercero=''; 
              Ext.getCmp(gridTercero).getStore().baseParams.nomtercero=''; 
              Ext.getCmp(gridTercero).getStore().load(); 

              Ext.getCmp(gridArticulo).getStore().removeAll();
              Ext.getCmp(gridArticulo).getStore().baseParams.idlista=idlista;
              Ext.getCmp(gridArticulo).getStore().baseParams.codarticulo=''; 
              Ext.getCmp(gridArticulo).getStore().baseParams.nomarticulo=''; 
              Ext.getCmp(gridArticulo).getStore().load();              

          }
    }
});

storeLista.on('load', function(store, records, options ) {   
     Ext.getCmp(gridTercero).getStore().removeAll();
     Ext.getCmp(gridArticulo).getStore().removeAll();
});  

storeArticulo.on('load', function(store, records, options ) {
    /*SeleccionaArticulosAprobados();*/
});  

var gridTercero= Ext.id();

var Pcl_Terceros = new Ext.grid.GridPanel({
    id: gridTercero,
    store:storeTercero,
    title:'Terceros',
    /*autoHeight:true,*/
    width:View.getBox().width*0.5,
    region:'east',
    loadMask:new Ext.LoadMask(Ext.getBody(), {msg:"Cargando terceros..."}),  
    columns: [		    
		    {
		        header:'CodTercero',
		        width:50,
		        dataIndex:'codtercero',
		        sortable: true
		    },        
		    {
                header:'Nombre Tercero',
                width:230,
                dataIndex:'nom_terc',
                sortable: true
            },
            {
		        header:'Es Predeterminada',
		        width:80,
		        dataIndex:'espredeterminada',
		        sortable: true
		    }
     ],
    border: true,
    stripeRows: true,  
    viewConfig: {
        forceFit: true
    },
    bbar: new Ext.PagingToolbar({
	    pageSize: 50,
	    store: storeTercero,
	    displayInfo: true,
	    displayMsg: '{0} - {1} de {2} Registros',
	    emptyMsg: "No hay registros"          
    }),
    listeners: {

    }
});

 var btnSave_Changes = new Ext.Button({
    icon:'images/database_save.png',
    cls:"x-btn-text-icon",
    text:'Guardar Cambios',
    hidden:false,
    tooltip: {
        title:'Guardar Cambios Realizados'
    },
    handler:function(){
        var selrecord=Ext.getCmp(gridlp).getSelectionModel().getSelected();
        var idlista=selrecord.get('idlista');
        GuardarCambiosArticulos(idlista);
    }
});
var smart = new Ext.grid.CheckboxSelectionModel({
    singleSelect:false,
    forceCtrl:true
});

var gridArticulo= Ext.id();

var Pcl_Articulos = new Ext.grid.GridPanel({
    id: gridArticulo,
    store:storeArticulo,    
    title:'Articulos',
   /* width:485,*/
    region:'center',
    loadMask:new Ext.LoadMask(Ext.getBody(), {msg:"Cargando articulos..."}),  
    columns: [		    
		    {
		        header:'CodArticulo',
		        width:50,
		        dataIndex:'codarticulo',
		        sortable: false
		    },        
		    {
                header:'Nombre Articulo',
                width:250,
                dataIndex:'nomarticulo',
                sortable: false
            }, 
            {
		        header:'Precio',
		        width:150,
		        dataIndex:'precio',
		        sortable: false
		    },        
            {
                header:'Aprobado',
                width:25,
                dataIndex:'idestado',
                sortable: false,
                renderer:function(value){
                    if(value=="POR APROBAR"){
                         return '<center><img style="cursor:pointer" src="images/checkbox_no.png"></center>'
                    }else{
                         return '<center><img style="cursor:pointer" src="images/checkbox_yes.png"></center>'
                    }
                    
                }
            }

            /*,smart*/
     ],
    border: true,
    stripeRows: true,
    /*sm:smart,*/
    viewConfig: {
        forceFit: true
    },
    bbar: new Ext.PagingToolbar({
	    pageSize: 50,
	    store: storeArticulo,
	    displayInfo: true,
	    displayMsg: '{0} - {1} de {2} Registros',
	    emptyMsg: "No hay registros",
        items:[{
               xtype: 'tbseparator'
        },btnSave_Changes]          
    }),
    listeners: {
        cellclick: function(grid, rowIndex, columnIndex, e) {
        
        var record = grid.getStore().getAt(rowIndex);  
        var fieldName = grid.getColumnModel().getDataIndex(columnIndex); 

        var data = record.get(fieldName);
      
        if(fieldName == "idestado"){
               if(data=="POR APROBAR"){                
                  record.set('idestado',"APROBADO");
              }else{              
                  record.set('idestado',"POR APROBAR");                  
              }
        }
        }
    }
});



var boxcampocodigo = new Ext.BoxComponent({
    autoEl: {
        html: 'Cod. Tercero: '
    }
});

var boxcampotercero = new Ext.BoxComponent({
    autoEl: {
        html: '&nbsp;&nbsp;Tercero: '
    }
});    

var south = {  
    xtype:"panel",  
    region:"south", 
    layout:"border",     
    height:View.getBox().height*0.5,
    items:[Pcl_Articulos] 
};  
  
var center = {  
    xtype:"panel",  
    region:"center",
    layout:"border",  
    items:[Pcl_ListaP,Pcl_Terceros,south] 
}; 



var panelPreciosAut = new Ext.Panel({  
   title:'Autorizaciones de precios',
    id:'grilla-precios-aut',
    closable:true,
    plugins:new SM.BaseObject(),  
    layout:"border",  
    height:View.getBox().height, 
    tbar:['  ',boxcampocodigo,{
        xtype:'textfield',
        id:'TxtCodigo',
        fieldLabel:'CodTercero',
        emptyText:'Busque por codigo ...',
        width:150,
        enableKeyEvents: true,
        listeners:{
            'keyup': function(el, e){               
                Ext.getCmp(gridlp).getStore().removeAll();                             
                Ext.getCmp(gridlp).getStore().baseParams.codtercero=el.getValue(); 
                Ext.getCmp(gridlp).getStore().baseParams.nomtercero=Ext.getCmp('TxtTercero').getValue(); 
                Ext.getCmp(gridlp).getStore().load(); 
            }
        }
    },
    boxcampotercero,{
        xtype:'textfield',
        id:'TxtTercero',
        fieldLabel:'Tercero',
        emptyText:'Busque por tercero ...',
        width:190,
        enableKeyEvents: true,
        listeners:{
            'keyup': function(el, e){               
                Ext.getCmp(gridlp).getStore().removeAll();                           
                Ext.getCmp(gridlp).getStore().baseParams.codtercero=Ext.getCmp('TxtCodigo').getValue(); 
                Ext.getCmp(gridlp).getStore().baseParams.nomtercero=el.getValue(); 
                Ext.getCmp(gridlp).getStore().load(); 
            }
        }
    }],
    items:[center]  
});  


function GuardarCambiosArticulos(idlista){
          
            var modificados = Ext.getCmp(gridArticulo).getStore().getModifiedRecords();          
            var datos = [];
            for(var i = 0; i < modificados.length; i++){
                 datos.push({
                    codarticulo:modificados[i].data.codarticulo,
                    idestado:modificados[i].data.idestado
                 });
            }
            var datosPrecios = Ext.encode(datos);
            Ext.Ajax.request({
                    url : 'Aplicaciones/ListaPrecio/CodigoOpciones/GuardarAprobacionPrecios.php',
                    params :{
                       idlista:idlista,
                       articulos:datosPrecios
                    },
                    success : function(response) {                         
                         Ext.getCmp(gridArticulo).getStore().baseParams.idlista=idlista;  
                         Ext.getCmp(gridArticulo).getStore().load();
                                                            
                        var respuesta=Ext.decode(response.responseText);
                        if(respuesta.success){
                              Ext.Msg.alert('Información','Cambios efectuados exitosamente');
                        }else{
                              Ext.Msg.alert('Información',respuesta.razon);
                        }
                         mascara.hide();
                    },
                    failure: function(){
                        Ext.Msg.alert('Error','Error al asignar terceros a la lista, intente más tarde');
                        mascara.hide();
                    }
            });
}

  function SeleccionaArticulosAprobados(){      
    
      Ext.each(storeArticulo, function(record) {
            for(var i=0;i<record.data.length;i++){
                if(record.data.items[i].data.idestado=='APROBADO'){   
                    Ext.getCmp(gridArticulo).getSelectionModel().selectRow(i,true);
                }
            }
            return false;
     });
  }

panelPreciosAut.SMRender();
panelPreciosAut.doLayout();
