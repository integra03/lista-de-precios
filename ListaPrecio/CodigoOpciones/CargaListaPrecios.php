<?php
	include("../../../Archivos/conectar_bd.php");
    session_start();
	$conexion1 = $_SESSION['conexion'];
	$datos=new bd($conexion1);
	set_error_handler("my_warning_handler", E_ALL);

  function my_warning_handler($errno, $errstr, $errfile, $errline, $errcontext) {
    throw new Exception( $errstr );
  }


        $start = isset($_POST['start'])?$_POST['start']:0;
        $limit = isset($_POST['limit'])?$_POST['limit']:50;

           if (isset($_POST['codtercero'])) {
                $codtercero = $_POST['codtercero'];
          }
          if (isset($_POST['nomtercero'])) {
                $nomtercero = $_POST['nomtercero'];
          }         
          if (isset($_POST['dir'])) {
            $dir = $_POST['dir'];
          }else {
            $dir = '';
          }
        
          $sqllp="SELECT ELP.IdEncListaPrecio as idenclistaprecio,ELP.nombre as nombre,ELP.descripcion as descripcion
                  FROM EncListaPrecios ELP
                  INNER JOIN ClientesListaPrecios CLP ON ELP.IdEncListaPrecio = CLP.IdEncListaPrecio
                  where ELP.IdEstadoEncLista = 'ACTIVO' ";
          if($codtercero!=''){
               $sqllp=$sqllp."  AND CLP.CodTercero like '%".$codtercero."%'";
          }
          if($nomtercero!=''){
                $sqllp=$sqllp." AND dbo.NombreTercero(CLP.CodTercero) like '%".$nomtercero."%'";
          } 
          $sqllp=$sqllp." GROUP BY ELP.IdEncListaPrecio,ELP.nombre,ELP.descripcion";
	        $sqldatos = "select * FROM(".$sqllp." ) T1 LEFT JOIN 
                  (
                  SELECT EL.IdEncListaPrecio AS idenclistaprecio2
                  FROM EncListaPrecios EL
                  INNER JOIN  DetListaPrecios DL ON DL.IdEncListaPrecio = EL.IdEncListaPrecio
                  WHERE DL.IdEstadoDetLista = 'POR APROBAR' and EL.IdEstadoEncLista = 'ACTIVO'
                  GROUP BY EL.IdEncListaPrecio
                  ) T2 ON T1.IdEncListaPrecio = T2.IdEncListaPrecio2 order by IdEncListaPrecio2 desc";
        
         
        if (isset($conexion1)) {
        //   $sqldatos=$sqldatos.$order;
           
            if (isset($_POST['sort'])) {
               $sortBy = $_POST['sort'];
               $order=" order by ".$sortBy." ".$dir;
               $sqldatos=$sqldatos.$order;
            }
            try{
                
                $result = $datos->consulta($sqldatos);

                while ($row = odbc_fetch_array($result)) {
                    $contenido['idlista'] = $row['idenclistaprecio'];
                    $contenido['nombre'] = utf8_encode($row['nombre']);  
                    $contenido['iddetlista'] = $row['idenclistaprecio2'];           
                    
                    $dato[] = $contenido;
                }
               
                if (!isset($dato)) {
                     $dato = null;
                     echo trim(json_encode(array('datoslista' => null, 'success' => true,'total' => count($dato))));
                }else{
                     echo trim(json_encode(array('datoslista' => array_splice($dato,$start,$limit), 'success' => true,'total' => count($dato))));
                }
                $datos->close();
            }catch (Exception $e) {
                      echo trim(json_encode(array('datoslista' => null, 'success' => false,'total' => 0)));
             }             
        } else {
            echo trim(json_encode(array('datoslista' => null, 'success' => false,'total' => 0)));
        }         
       
	
?>