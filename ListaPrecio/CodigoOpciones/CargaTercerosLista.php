<?php
	include("../../../Archivos/conectar_bd.php");
    session_start();
	$conexion1 = $_SESSION['conexion'];
	$datos=new bd($conexion1);
	set_error_handler("my_warning_handler", E_ALL);

  function my_warning_handler($errno, $errstr, $errfile, $errline, $errcontext) {
    throw new Exception( $errstr );
  }

	if (isset($_POST['idlista'])) {
        $start = isset($_POST['start'])?$_POST['start']:0;
        $limit = isset($_POST['limit'])?$_POST['limit']:50;

          if (isset($_POST['codtercero'])) {
                $codtercero = $_POST['codtercero'];
          }
          if (isset($_POST['nomtercero'])) {
                $nomtercero = $_POST['nomtercero'];
          }         
          if (isset($_POST['dir'])) {
            $dir = $_POST['dir'];
          }else {
            $dir = '';
          }
        

	       $idlista = $_POST['idlista']; 
          $sqldatos = "select CodTercero as codtercero, nom_terc,dbo.EsListaPredeterminada(codtercero,".$idlista.") as espredet from g0BNits where codtercero in(
          select codtercero from ClientesListaPrecios where IdEncListaPrecio=".$idlista.")";

         
        if (isset($conexion1)) {
        //   $sqldatos=$sqldatos.$order;
            if($codtercero!=''){
               $sqldatos=$sqldatos." and codtercero like '%".$codtercero."%'";
            }
            if($nomtercero!=''){
                $sqldatos=$sqldatos." and nom_terc like'%".$nomtercero."%'";
            }
            if (isset($_POST['sort'])) {
               $sortBy = $_POST['sort'];
               $order=" order by ".$sortBy." ".$dir;
               $sqldatos=$sqldatos.$order;
            }
            try{
                $result = $datos->consulta($sqldatos);

                while ($row = odbc_fetch_array($result)) {
                    $contenido['codtercero'] = $row['codtercero'];
                    $contenido['nom_terc'] = utf8_encode($row['nom_terc']);
                    $contenido['espredeterminada'] = utf8_encode($row['espredet']);             
                    
                    $dato[] = $contenido;
                }
               
                if (!isset($dato)) {
                     $dato = null;
                     echo trim(json_encode(array('datosterc' => null, 'success' => true,'total' => count($dato))));
                }else{
                     echo trim(json_encode(array('datosterc' => array_splice($dato,$start,$limit), 'success' => true,'total' => count($dato))));
                }
                $datos->close();
            }catch (Exception $e) {
                      echo trim(json_encode(array('datosterc' => null, 'success' => false,'total' => 0)));
             }   
            //echo "{success:true,datosterc:" . json_encode($dato) . ",maxtercero:".$maxterc."}";
        } else {
            echo trim(json_encode(array('datosterc' => null, 'success' => false,'total' => 0)));
        }         
       
	}else{
     echo trim(json_encode(array('datosterc' => null, 'success' => false,'total' => 0)));
		 
	}
?>