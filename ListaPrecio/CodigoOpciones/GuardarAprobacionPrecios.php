<?php
	include("../../../Archivos/conectar_bd.php");
    session_start();
	$conexion1 = $_SESSION['conexion'];
	$datos=new bd($conexion1);
    set_error_handler("my_warning_handler", E_ALL);

	function my_warning_handler($errno, $errstr, $errfile, $errline, $errcontext) {
		throw new Exception( $errstr );
	}

	if (isset($_POST['idlista'])) {
	    $idlista = $_POST['idlista'];
        $datosArt = $_POST['articulos'];        
      
        $sqlupdart='';
        $datosArticulos = json_decode($datosArt, true);
        for ($i = 0; $i < count($datosArticulos); $i++) {
              $sqlupdart .= "update DetListaPrecios set idestadodetlista='" . $datosArticulos[$i]['idestado'] . "' where ".
              " codarticulo='" . $datosArticulos[$i]['codarticulo'] . "' and idenclistaprecio=".$idlista.";";           
        }
       try{
       	   
       	   $datos->consulta($sqlupdart);
            echo json_encode(array(
			    'success' => true,
			    'razon' => 'Cambios guardados satisfactoriamente'			    
	       ));  
       }catch (Exception $e) {       	
       	   echo json_encode(array(
			    'success' => false,
			    'razon' => 'Error al guardar cambios a la lista de precios'			    
	       ));  
       }
	}else{
		 echo json_encode(array(
			    'success' => false,
			    'razon' => 'Error al guardar cambios a la lista de precios'			    
	     ));  
	}
?>